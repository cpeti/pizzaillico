﻿using System;
using System.Windows.Input;
using FluentValidation.Results;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utilities;
using PizzaIllico.Mobile.Validations;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        private UserProfileResponse _profile;
        private string _errorLastName;
        private string _errorFirstName;
        private string _errorPhoneNumber;
        private string _errorEmail;
        private bool _isLoading;

        public UserProfileResponse Profile
        {
            get => _profile;
            set => SetProperty(ref _profile, value);
        }
        
        public string ErrorLastName
        {
            get => _errorLastName;
            set => SetProperty(ref _errorLastName, value);
        }

        public string ErrorFirstName
        {
            get => _errorFirstName;
            set =>
                SetProperty(ref _errorFirstName, value);
        }

        public string ErrorPhoneNumber
        {
            get => _errorPhoneNumber;
            set => SetProperty(ref _errorPhoneNumber, value);
        }

        public string ErrorEmail
        {
            get => _errorEmail;
            set => SetProperty(ref _errorEmail, value);
        }

        public bool IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        public ICommand SetUserProfileCommand { get; }
        public ICommand GoToSetPasswordPageCommand { get; }
        
        public ICommand LogoutCommand { get; }

        public ProfileViewModel()
        {
            IsLoading = true;
            GetUserProfile();
            
            SetUserProfileCommand = new Command(SetUserProfile);
            GoToSetPasswordPageCommand = new Command(GoToSetPasswordPage);
            LogoutCommand = new Command(Logout);
        }

        private async void GetUserProfile()
        {
            IsLoading = true;
            try
            {
                TokenHandler.CheckToken();
                IUserApiService service = DependencyService.Get<IUserApiService>();

                string token = await TokenHandler.GetAuthorization();
                Response<UserProfileResponse> response = await service.GetUserProfile(token);

                if (response.IsSuccess)
                {
                    Profile = response.Data;
                }
                IsLoading = false;
            }
            catch (Exception)
            {
                IsLoading = false;
                if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
            }
        }

        private async void SetUserProfile()
        {
            IsLoading = true;
            try
            {
                TokenHandler.CheckToken();
                
                string token = await TokenHandler.GetAuthorization();

                SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest()
                {
                    Email = Profile.Email,
                    FirstName = Profile.FirstName,
                    LastName = Profile.LastName,
                    PhoneNumber = Profile.PhoneNumber
                };

                if (Validate())
                {
                    IUserApiService service = DependencyService.Get<IUserApiService>();

                    Response<UserProfileResponse> response = await service.SetUserProfile(setUserProfileRequest, token);
                    IsLoading = false;

                    if (response.IsSuccess)
                    {
                        IDialogService dialogService = DependencyService.Get<IDialogService>();
                        await dialogService.DisplayAlertAsync("Profil", "Votre profil a été modifié.", "OK");
                    }
                    else if (response.ErrorCode == "GENERIC_HTTP_ERROR")
                    {
                        IDialogService dialogService = DependencyService.Get<IDialogService>();
                        await dialogService.DisplayAlertAsync("Erreur", "Veuillez vous reconnecter.", "OK");
                        TokenHandler.RemoveAll();
                        INavigationService navigationService = DependencyService.Get<INavigationService>();
                        await navigationService.PopAsync();
                    }   
                }
                else
                {
                    IsLoading = false;
                }
            }
            catch (Exception)
            {
                IsLoading = false;
                if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
            }
        }
        
        private void GoToSetPasswordPage()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<SetPasswordPage>();
        }

        private void Logout()
        {
            TokenHandler.RemoveAll();
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PopAsync();
        }

        private bool Validate()
        {
            ErrorEmail = "";
            ErrorFirstName = "";
            ErrorLastName = "";
            ErrorPhoneNumber = "";
            
            UserProfileResponseValidator validator = new UserProfileResponseValidator();

            ValidationResult result = validator.Validate(_profile);
            
            if(!result.IsValid) {
                foreach(var failure in result.Errors) {
                    switch (failure.PropertyName)
                    {
                        case "Email":
                            ErrorEmail = failure.ErrorMessage;
                            break;
                        case "LastName":
                            ErrorLastName = failure.ErrorMessage;
                            break;
                        case "FirstName":
                            ErrorFirstName = failure.ErrorMessage;
                            break;
                        case "PhoneNumber":
                            ErrorPhoneNumber = failure.ErrorMessage;
                            break;
                    }
                }
                return false;
            }
            return true;
        }
    }
}