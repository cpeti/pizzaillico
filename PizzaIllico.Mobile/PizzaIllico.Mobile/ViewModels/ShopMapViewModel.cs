﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PizzaIllico.Mobile.CustomRenderers;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Map = Xamarin.Forms.Maps.Map;

namespace PizzaIllico.Mobile.ViewModels
{
    public class ShopMapViewModel : ViewModelBase
    {
       private Collection<ShopItem> _shops;
       private bool _isLoading;

       public Map MapView { get; }

       public bool IsLoading
       {
           get => _isLoading;
           set => SetProperty(ref _isLoading, value);
       }

       public ShopMapViewModel()
       {
           IsLoading = true;
            // position par défaut de la map
            Position position = new Position(46.8588888, 2.4956642);
            MapSpan mapSpan = new MapSpan(position, 6, 6);
            MapView = new Map(mapSpan);
            MapView.IsShowingUser = true;
            
            AddPins();
            IsLoading = false;
       }

        private async void AddPins()
        {
            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

            try
            {
                Response<List<ShopItem>> response = await service.ListShops();
                if (response.IsSuccess)
                {
                    _shops = new ObservableCollection<ShopItem>(response.Data);
                    foreach (ShopItem shop in _shops)
                    {
                        ShopPin pin = new ShopPin
                        {
                            ShopId = shop.Id,
                            Label = shop.Name,
                            Address = shop.Address,
                            Type = PinType.Place,
                            Position = new Position(shop.Latitude, shop.Longitude)
                        };
                        pin.InfoWindowClicked += (s, args) =>
                        {
                            ShopPin shopPin = (ShopPin) s;
                            INavigationService navigationService = DependencyService.Get<INavigationService>();
                            navigationService.PushAsync<PizzaListPage>(
                                new Dictionary<string, object>()
                                {
                                    {"ShopId", Convert.ToInt32(shopPin.ShopId)},
                                    {"ShopName", shopPin.Label},
                                }
                            );
                        };
                        MapView.Pins.Add(pin);
                    }
                }
            }
            catch (Exception)
            {
                if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
            }
        }
    }
}