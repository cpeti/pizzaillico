﻿using System.Windows.Input;
using PizzaIllico.Mobile.Utilities;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class HomeTabbedPageViewModel : ViewModelBase
    {
        public ICommand ShowDialogCommand { get; }

        public HomeTabbedPageViewModel()
        {
            ShowDialogCommand = new Command(ShowDialog);
        }

        private async void ShowDialog()
        {
            IDialogService dialogService = DependencyService.Get<IDialogService>();
            bool quit = await dialogService.DisplayAlertAsync("Confirmation de déconnexion", "Êtes-vous sûr de vouloir vous déconnecter?",
                "Oui", "Annuler");

            if (quit)
            {
                INavigationService navigationService = DependencyService.Get<INavigationService>();
                await navigationService.PopAsync();
                TokenHandler.RemoveAll();
            }
        }
        
        
    }
}