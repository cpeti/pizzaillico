using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class ShopListViewModel : ViewModelBase
    {
	    private bool _isLoading;
	    private ObservableCollection<ShopItem> _shops;
	    private Location _userLocation;

	    public ObservableCollection<ShopItem> Shops
	    {
		    get => _shops;
		    set => SetProperty(ref _shops, value);
	    }

	    public bool IsLoading
	    {
		    get => _isLoading;
		    set => SetProperty(ref _isLoading, value);
	    }

	    public Location UserLocation
	    {
		    get => _userLocation;
		    set => SetProperty(ref _userLocation, value);
	    }

	    public ICommand SelectedCommand { get; }

	    public ShopListViewModel()
	    {
		    IsLoading = true;
		    SelectedCommand = new Command<ShopItem>(SelectedAction);
	    }

	    private void SelectedAction(ShopItem obj)
	    {
		    // Send shop id to PizzaListPage
		    NavigationService.PushAsync<PizzaListPage>(
			    new Dictionary<string, object>()
			    {
				    {"ShopId", Convert.ToInt32(obj.Id)},
				    {"ShopName", obj.Name}
			    });
	    }

	    public override async Task OnResume()
        {
	        await base.OnResume();

	        GetUserLocation();
	        
	        IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

	        try
	        {
		        Response<List<ShopItem>> response = await service.ListShops();
		        IsLoading = false;
		        if (response.IsSuccess)
		        {
			        Shops = new ObservableCollection<ShopItem>(response.Data);
		        }
	        }
	        catch (Exception)
	        {
		        if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    IsLoading = false;
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
	        }
        }

	    private async void GetUserLocation()
	    {
		    try
		    {
			    UserLocation = await Geolocation.GetLastKnownLocationAsync();
		    }
		    catch (FeatureNotSupportedException fnsEx)
		    {
			    // Handle not supported on device exception
		    }
		    catch (FeatureNotEnabledException fneEx)
		    {
			    // Handle not enabled on device exception
		    }
		    catch (PermissionException pEx)
		    {
			    // Handle permission exception
		    }
		    catch (Exception ex)
		    {
			    // Unable to get location
		    }
	    }
    }
}