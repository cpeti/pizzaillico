﻿using System;
using System.Windows.Input;
using FluentValidation.Results;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utilities;
using PizzaIllico.Mobile.Validations;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class SetPasswordViewModel : ViewModelBase
    {
        private SetPasswordRequest _setPasswordRequest;

        private string _oldPassword;
        private string _newPassword;
        private string _confirmPassword;
        
        private string _errorOldPassword;
        private string _errorNewPassword;
        private string _errorConfirmPassword;

        public string OldPassword
        {
            get => _oldPassword;
            set => SetProperty(ref _oldPassword, value);
        }

        public string NewPassword
        {
            get => _newPassword;
            set => SetProperty(ref _newPassword, value);
        }

        public string ConfirmPassword
        {
            get => _confirmPassword;
            set => SetProperty(ref _confirmPassword, value);
        }
        
        public string ErrorOldPassword
        {
            get => _errorOldPassword;
            set => SetProperty(ref _errorOldPassword, value);
        }
        
        public string ErrorNewPassword
        {
            get => _errorNewPassword;
            set => SetProperty(ref _errorNewPassword, value);
        }
        
        public string ErrorConfirmPassword
        {
            get => _errorConfirmPassword;
            set => SetProperty(ref _errorConfirmPassword, value);
        }

        public ICommand SetPasswordCommand { get; }

        public SetPasswordViewModel()
        {
            SetPasswordCommand = new Command(SetPassword);
        }

        private async void SetPassword()
        {
            try
            {
                TokenHandler.CheckToken();

                string token = await TokenHandler.GetAuthorization();

                _setPasswordRequest = new SetPasswordRequest()
                {
                    NewPassword = NewPassword,
                    OldPassword = OldPassword
                };

                if (Validate())
                {
                    IAuthApiService service = DependencyService.Get<IAuthApiService>();
                    Response<LoginResponse> response = await service.SetPassword(_setPasswordRequest, token);

                    if (response.IsSuccess)
                    {
                        IDialogService dialogService = DependencyService.Get<IDialogService>();
                        await dialogService.DisplayAlertAsync("Mot de passe", "Votre mot de passe a été modifié.", "OK");
                    }
                    else
                    {
                        HandleErrorCode(response.ErrorCode);
                    }
                }
            }
            catch (Exception)
            {
                if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
            }
            ResetEntries();
        }

        private void HandleErrorCode(string errorCode)
        {
            switch (errorCode)
            {
                case "INVALID_PASSWORD":
                    ErrorOldPassword = "Le mot de passe est incorrect.";
                    break;
                case "WEAK_PASSWORD":
                    ErrorNewPassword = "Le mot de passe doit contenir au moins 8 caractères.";
                    break;
            }
        }

        private bool Validate()
        {
            ErrorConfirmPassword = "";
            ErrorNewPassword = "";
            ErrorOldPassword = "";
            
            bool isValid = true;
            
            SetPasswordRequestValidator validator = new SetPasswordRequestValidator();

            ValidationResult result = validator.Validate(_setPasswordRequest);
            
            if(!result.IsValid) {
                foreach(var failure in result.Errors) {
                    switch (failure.PropertyName)
                    {
                        case "OldPassword":
                            ErrorOldPassword = failure.ErrorMessage;
                            break;
                        case "NewPassword":
                            ErrorNewPassword = failure.ErrorMessage;
                            break;
                    }
                }
                isValid = false;
            }

            if (ConfirmPassword == null)
            {
                ErrorConfirmPassword = "Le champ est obligatoire.";
                isValid = false;
            }
            else if (!ConfirmPassword.Equals(NewPassword))
            {
                ErrorConfirmPassword = "Les mots de passe ne correspondent pas";
                isValid = false;
            }

            return isValid;
        }

        private void ResetEntries()
        {
            OldPassword = "";
            NewPassword = "";
            ConfirmPassword = "";
        }
    }
}