﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utilities;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _email;
        private string _password;
        private bool _isloading;
        private LoginWithCredentialsRequest _loginDetails;

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public bool Isloading
        {
            get => _isloading;
            set => SetProperty(ref _isloading, value);
        }

        public ICommand GoToSignUpPageCommand { get; }
        public ICommand LoginCommand { get; }

        public LoginViewModel()
        {
            GoToSignUpPageCommand = new Command(GoToSignUpPage);
            LoginCommand = new Command(Login);
            Isloading = false;
        }

        private void GoToSignUpPage()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<SignUpPage>();
        }
        
        private async void Login()
        {
            _loginDetails = new LoginWithCredentialsRequest()
            {
                ClientId = Keys.CLIENT_ID,
                ClientSecret = Keys.CLIENT_SECRET,
                Login = Email,
                Password = Password
            };
            
            IAuthApiService service = DependencyService.Get<IAuthApiService>();
            Isloading = true;
            try
            {
                Response<LoginResponse> response = await service.Login(_loginDetails);

                if (response.IsSuccess)
                {
                    LoginResponse data = response.Data;
                    try
                    {
                        TokenHandler.SaveLoginResponse(data);
                        GoToShopMapPage();
                        await Task.Run(async () =>
                        {
                            Isloading = false;
                            Email = "";
                            Password = "";
                        });
                    }
                    catch (Exception)
                    {
                        Isloading = false;
                        IDialogService dialogService = DependencyService.Get<IDialogService>();
                        await dialogService.DisplayAlertAsync("Oups", "Erreur interne.", "OK");
                    }
                } 
                else if (response.ErrorCode == "GENERIC_HTTP_ERROR")
                {
                    Isloading = false;
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Adresse e-mail et/ou mot de passe incorrect(s).", "OK");
                }
            }
            catch (Exception)
            {
                if (Connectivity.NetworkAccess == NetworkAccess.None)
                {
                    Isloading = false;
                    IDialogService dialogService = DependencyService.Get<IDialogService>();
                    await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                }
            }
        }

        private void GoToShopMapPage()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<HomeTabbedPage>();
        }
    }
}