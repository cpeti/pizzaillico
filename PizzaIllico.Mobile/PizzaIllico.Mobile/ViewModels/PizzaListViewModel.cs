﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Storage;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class PizzaListViewModel : ViewModelBase
    {
        private ObservableCollection<PizzaItem> _pizzas;

        private int _shopId;
        private string _shopName;

        [NavigationParameter]
        public int ShopId
        {
            get => _shopId;
            set => SetProperty(ref _shopId, value);
        }
        [NavigationParameter]
        public string ShopName
        {
            get => _shopName;
            set => SetProperty(ref _shopName, value);
        }

        public ObservableCollection<PizzaItem> Pizzas
        {
            get => _pizzas;
            set => SetProperty(ref _pizzas, value);
        }

        public ICommand OrderCommand { get; }

        public PizzaListViewModel()
        {
            OrderCommand = new Command<PizzaItem>(OrderAction);
        }

        private async void OrderAction(PizzaItem obj)
        {
            if (OrderStorage.AddPizzaOrder(ShopId, ShopName, obj))
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Ajoutée !", "la pizza " + obj.Name + " est ajoutée dans le panier.", "OK");
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Erreur", "Vous ne pouvez pas commander sur plusieurs restorants.", "OK");

            }

        }
        
        

        public override async Task OnResume()
        {
            await base.OnResume();

            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

            Response<List<PizzaItem>> response = await service.ListPizzas(ShopId);

            if (response.IsSuccess)
            {
                Pizzas = new ObservableCollection<PizzaItem>(response.Data);
                foreach (PizzaItem pizza in Pizzas)
                {
                    string imgUrl = Urls.OVH + Urls.GET_IMAGE.Replace(Urls.SHOP_ID, ShopId.ToString())
                        .Replace(Urls.PIZZA_ID, pizza.Id.ToString());
                    pizza.ImageUrl = imgUrl;
                }
            }
        }
    }
}