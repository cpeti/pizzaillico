﻿using System;
using System.Windows.Input;
using FluentValidation.Results;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utilities;
using PizzaIllico.Mobile.Validations;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        private CreateUserRequest _newUser;
        private string _lastName;
        private string _firstName;
        private string _phoneNumber;
        private string _email;
        private string _password;
        private string _confirmPassword;
        private string _errorLastName;
        private string _errorFirstName;
        private string _errorPhoneNumber;
        private string _errorEmail;
        private string _errorPassword;
        private string _errorConfirmPassword;
        private bool _isLoading;

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        public string PhoneNumber
        {
            get => _phoneNumber;
            set => SetProperty(ref _phoneNumber, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string ConfirmPassword
        {
            get => _confirmPassword;
            set => SetProperty(ref _confirmPassword, value);
        }
        
        public string ErrorLastName
        {
            get => _errorLastName;
            set => SetProperty(ref _errorLastName, value);
        }

        public string ErrorFirstName
        {
            get => _errorFirstName;
            set => SetProperty(ref _errorFirstName, value);
        }

        public string ErrorPhoneNumber
        {
            get => _errorPhoneNumber;
            set => SetProperty(ref _errorPhoneNumber, value);
        }

        public string ErrorEmail
        {
            get => _errorEmail;
            set => SetProperty(ref _errorEmail, value);
        }

        public string ErrorPassword
        {
            get => _errorPassword;
            set => SetProperty(ref _errorPassword, value);
        }

        public string ErrorConfirmPassword
        {
            get => _errorConfirmPassword;
            set => SetProperty(ref _errorConfirmPassword, value);
        }

        public bool IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        public ICommand OnSignUpCommand { get; }

        public SignUpViewModel()
        {
            IsLoading = false;
            OnSignUpCommand = new Command(SignUp);
        }
        
        private async void SignUp()
        {
            _newUser = new CreateUserRequest()
            {
                ClientId = "MOBILE",
                ClientSecret = "UNIV",
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                Password = Password,
                PhoneNumber = PhoneNumber
            };
            
            if (Validate())
            {
                IUserApiService service = DependencyService.Get<IUserApiService>();

                try
                {
                    IsLoading = true;
                    Response<LoginResponse> response = await service.Register(_newUser);

                    if (response.IsSuccess)
                    {
                        LoginResponse data = response.Data;
                        try
                        {
                            TokenHandler.SaveLoginResponse(data);
                            GoToHomePage();
                            IsLoading = false;
                            ResetEntries();
                        }
                        catch (Exception)
                        {
                            IsLoading = false;
                            IDialogService dialogService = DependencyService.Get<IDialogService>();
                            await dialogService.DisplayAlertAsync("Oups", "Erreur interne.", "OK");
                        }
                    }
                }
                catch (Exception)
                {
                    IsLoading = false;
                    if (Connectivity.NetworkAccess == NetworkAccess.None)
                    {
                        IDialogService dialogService = DependencyService.Get<IDialogService>();
                        await dialogService.DisplayAlertAsync("Erreur", "Aucune connexion internet.", "OK");
                    }
                }   
            }
        }

        private bool Validate()
        {
            ErrorEmail = "";
            ErrorPassword = "";
            ErrorConfirmPassword = "";
            ErrorFirstName = "";
            ErrorLastName = "";
            ErrorPhoneNumber = "";

            bool isValid = true;
            
            CreateUserRequestValidator validator = new CreateUserRequestValidator();

            ValidationResult result = validator.Validate(_newUser);
            
            if(!result.IsValid) {
                foreach(var failure in result.Errors) {
                    switch (failure.PropertyName)
                    {
                        case "Email":
                            ErrorEmail = failure.ErrorMessage;
                            break;
                        case "LastName":
                            ErrorLastName = failure.ErrorMessage;
                            break;
                        case "FirstName":
                            ErrorFirstName = failure.ErrorMessage;
                            break;
                        case "PhoneNumber":
                            ErrorPhoneNumber = failure.ErrorMessage;
                            break;
                        default:
                            ErrorPassword = failure.ErrorMessage;
                            break;
                    }
                }
                isValid = false;
            }

            if (ConfirmPassword == null)
            {
                ErrorConfirmPassword = "Le champ est obligatoire.";
                isValid = false;
            } 
            else if (!ConfirmPassword.Equals(Password))
            {
                ErrorConfirmPassword = "Les mots de passe ne correspondent pas.";
                isValid = false;
            };

            return isValid;
        }

        private async void GoToHomePage()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<HomeTabbedPage>();
        }
        private void ResetEntries()
        {
            _newUser = new CreateUserRequest();
            LastName = "";
            FirstName = "";
            PhoneNumber = "";
            Email = "";
            Password = "";
            ConfirmPassword = "";
        }
    }
}