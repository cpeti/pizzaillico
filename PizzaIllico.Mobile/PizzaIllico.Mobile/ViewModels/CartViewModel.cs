﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MonkeyCache.SQLite;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Storage;
using PizzaIllico.Mobile.Utilities;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class CartViewModel : ViewModelBase
    {
        private ObservableCollection<PizzaItem> _pizzas;
        private long _shopId;
        private string _shopName;
        public bool _isNotEmptyData;
        public double _totalPrice;
        public long ShopId
        {
            get => _shopId;
            set => SetProperty(ref _shopId, value);
        }
        public string ShopName
        {
            get => _shopName;
            set => SetProperty(ref _shopName, value);
        }

        public bool IsNotEmptyData
        {
            get => _isNotEmptyData;
            set => SetProperty(ref _isNotEmptyData, value);
        }

        public double TotalPrice
        {
            get => _totalPrice;
            set => SetProperty(ref _totalPrice, value);
        }

        public ObservableCollection<PizzaItem> Pizzas
        {
            get => _pizzas;
            set => SetProperty(ref _pizzas, value);
        }

        public ICommand SendOrderCommand { get; }
        public ICommand EmptyAllOrderCommand { get; }
        public ICommand RemovePizzaCommand { get; }
        public ICommand GoRestoCommand { get; }

        public CartViewModel()
        {
            SendOrderCommand = new Command(SendOrderAction);
            EmptyAllOrderCommand = new Command(EmptyAllOrderAction);
            RemovePizzaCommand = new Command<long>(RemovePizzaAction);
            GoRestoCommand = new Command(GoRestoAction);
        }

        private async void SendOrderAction()
        {
            Collection<PizzaItem> pizzas = OrderStorage.GetPizzaOrder();
            List<long> pizzasId = new List<long>();
            foreach (PizzaItem pizza in pizzas)
            {
                pizzasId.Add(pizza.Id);
                Console.WriteLine(pizza.Id);
            }

            CreateOrderRequest orderRequest = new CreateOrderRequest()
            {
                PizzaIds = pizzasId
            };
            
            IOrderApiService service = DependencyService.Get<IOrderApiService>();
            TokenHandler.CheckToken();
            string token = await TokenHandler.GetAuthorization();
            Response<OrderItem> response = await service.DoOrder(ShopId, orderRequest, token);            
            if (response.IsSuccess)
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Commande envoyée !", "", "OK");
                EmptyAllOrderAction();
            }
        }
        
        private void EmptyAllOrderAction()
        {
            OrderStorage.EmptyOrder();
            Reload();
        }

        private void RemovePizzaAction(long pizzaId)
        {
            OrderStorage.RemovePizza(pizzaId);
            Reload();
        }

        private void GoRestoAction()
        {
            NavigationService.PushAsync<PizzaListPage>(
                new Dictionary<string, object>()
                {
                    {"ShopId", Convert.ToInt32(ShopId)},
                    {"ShopName", ShopName}
                });
        }

        
        public void Reload()
        {
            Collection<PizzaItem> pizzas = OrderStorage.GetPizzaOrder();
            TotalPrice = 0;

            if (pizzas != null)
            {
                Pizzas = new ObservableCollection<PizzaItem>(pizzas);
                foreach (PizzaItem pizza in Pizzas)
                {
                    TotalPrice += pizza.Price;
                }
            }
            else
            {
                Pizzas = null;
            }
            ShopId = OrderStorage.GetShopOrderId();
            ShopName = OrderStorage.GetShopOrderName();
            IsNotEmptyData = Pizzas != null && Pizzas.Count > 0;

        }
        
        public override async Task OnResume()
        {
            await base.OnResume();
            Reload();
        }
    }
}