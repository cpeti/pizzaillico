﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utilities;
using Storm.Mvvm;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    public class OrderListViewModel : ViewModelBase
    {
        private ObservableCollection<OrderItem> _orderItems;

        public ObservableCollection<OrderItem> OrderItems
        {
            get => _orderItems;
            set => SetProperty(ref _orderItems, value);
        }
        
        public override async Task OnResume()
        {
            await base.OnResume();
            IOrderApiService service = DependencyService.Get<IOrderApiService>();
            TokenHandler.CheckToken();
            string token = await TokenHandler.GetAuthorization();
            Response<List<OrderItem>> response = await service.ListOrders(token);
            if (response.IsSuccess)
            {
                OrderItems = new ObservableCollection<OrderItem>(response.Data);
            }
        }
    }
}