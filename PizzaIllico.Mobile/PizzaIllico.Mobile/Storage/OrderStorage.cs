﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using MonkeyCache;
using MonkeyCache.SQLite;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Essentials;

namespace PizzaIllico.Mobile.Storage
{
    public class OrderStorage
    {
        private static string KEY_ORDER = "order";
        private static string KEY_SHOP_ID = "shopId";
        private static string KEY_SHOP_NAME = "shopName";

        public static bool AddPizzaOrder(long shopId, string shopName, PizzaItem pizza)
        {
            // first order
            if (GetPizzaOrder() == null)
            {
                // add shopId and shopName
                Barrel.Current.Add(key: KEY_SHOP_ID, data: shopId, expireIn: TimeSpan.FromDays(1));
                Barrel.Current.Add(key: KEY_SHOP_NAME, data: shopName, expireIn: TimeSpan.FromDays(1));
                
                Collection<PizzaItem> pizzaItems = new Collection<PizzaItem>();
                pizzaItems.Add(pizza);
                Barrel.Current.Add(key: KEY_ORDER, data: pizzaItems, expireIn: TimeSpan.FromDays(1));
                return true;
            }
            else if (GetShopOrderId().Equals(shopId) && GetShopOrderName().Equals(shopName))
            {
                Collection<PizzaItem> pizzaItems = Barrel.Current.Get<Collection<PizzaItem>>(KEY_ORDER);
                pizzaItems.Add(pizza);
                Barrel.Current.Add(key: KEY_ORDER, data: pizzaItems, expireIn: TimeSpan.FromDays(1));
                return true;
            }
            return false;
        }

        public static Collection<PizzaItem> GetPizzaOrder()
        {
            return Barrel.Current.Exists(KEY_ORDER)? Barrel.Current.Get<Collection<PizzaItem>>(key: KEY_ORDER): null;
        }

        public static void RemovePizza(long pizzaId)
        {
            Collection<PizzaItem> pizzas = Barrel.Current.Get<Collection<PizzaItem>>(key: KEY_ORDER);
            foreach (PizzaItem pizza in pizzas)
            {
                if (pizza.Id.Equals(pizzaId))
                {
                    // last element
                    if (pizzas.Count == 1)
                    {
                        EmptyOrder();
                    }
                    else
                    {
                        pizzas.Remove(pizza);
                        Barrel.Current.Add(key: KEY_ORDER, data: pizzas, expireIn: TimeSpan.FromDays(1));
                    }
                    break;
                }
            }
        }
        
        public static long GetShopOrderId()
        {
            return Barrel.Current.Exists(KEY_SHOP_ID)? Barrel.Current.Get<long>(key: KEY_SHOP_ID): -1;
        }

        public static string GetShopOrderName()
        {
            return Barrel.Current.Exists(KEY_SHOP_NAME)? Barrel.Current.Get<string>(key: KEY_SHOP_NAME): "";
        }

        public static void EmptyOrder()
        {
            Barrel.Current.Empty(KEY_ORDER);
            Barrel.Current.Empty(KEY_SHOP_ID);
            Barrel.Current.Empty(KEY_SHOP_NAME);
        }
    }

}