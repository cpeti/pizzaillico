﻿using System;
using System.Threading.Tasks;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Utilities
{
    public static class TokenHandler
    {
        public static async void CheckToken()
        {
            string expiresIn = await SecureStorage.GetAsync(Keys.EXPIRES_DATE);

            if (DateTime.Now >= DateTime.Parse(expiresIn))
            {
                string refreshToken = await SecureStorage.GetAsync(Keys.REFRESH_TOKEN);

                RefreshRequest refreshRequest = new RefreshRequest()
                {
                    ClientId = Keys.CLIENT_ID,
                    ClientSecret = Keys.CLIENT_SECRET,
                    RefreshToken = refreshToken
                };

                IAuthApiService service = DependencyService.Get<IAuthApiService>();
                Response<LoginResponse> response = await service.RefreshToken(refreshRequest);
                if (response.IsSuccess)
                {
                    LoginResponse data = response.Data;
                    await SecureStorage.SetAsync(Keys.ACCESS_TOKEN, data.AccessToken);
                    await SecureStorage.SetAsync(Keys.REFRESH_TOKEN, data.RefreshToken);
                    await SecureStorage.SetAsync(Keys.TOKEN_TYPE, data.TokenType);
                    await SecureStorage.SetAsync(Keys.EXPIRES_DATE, DateTime.Now.AddSeconds(data.ExpiresIn).ToString());
                }
            }
        }
        
        public static async Task<string> GetAuthorization()
        {
            string accessToken = await SecureStorage.GetAsync(Keys.ACCESS_TOKEN);
            string tokenType = await SecureStorage.GetAsync(Keys.TOKEN_TYPE);
            return tokenType + " " + accessToken;
        }

        public static async void SaveLoginResponse(LoginResponse data)
        {
            await SecureStorage.SetAsync(Keys.ACCESS_TOKEN, data.AccessToken);
            await SecureStorage.SetAsync(Keys.REFRESH_TOKEN, data.RefreshToken);
            await SecureStorage.SetAsync(Keys.TOKEN_TYPE, data.TokenType);
            await SecureStorage.SetAsync(Keys.EXPIRES_DATE, DateTime.Now.AddSeconds(data.ExpiresIn).ToString());
        }

        public static void RemoveAll()
        {
            SecureStorage.RemoveAll();
        }
    }
}