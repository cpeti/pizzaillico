﻿namespace PizzaIllico.Mobile.Utilities
{
    public static class Keys
    {
        public const string ACCESS_TOKEN = "access_token";
        public const string REFRESH_TOKEN = "refresh_token";
        public const string TOKEN_TYPE = "token_type";
        public const string EXPIRES_DATE = "expires_date";
        public const string CLIENT_ID = "MOBILE";
        public const string CLIENT_SECRET = "UNIV";
    }
}