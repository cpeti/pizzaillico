﻿using PizzaIllico.Mobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PizzaListPage
    {
        public PizzaListPage()
        {
            BindingContext = new PizzaListViewModel();
            InitializeComponent();
        }
    }
}