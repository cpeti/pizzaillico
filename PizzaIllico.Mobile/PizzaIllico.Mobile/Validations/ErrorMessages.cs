﻿namespace PizzaIllico.Mobile.Validations
{
    public static class ErrorMessages
    {
        public static string REQUIRED_FIELD = "Le champ est obligatoire.";
        public static string INCORRECT_EMAIL = "L'adresse e-mail est incorrecte.";
        public static string INCORRECT_PHONE_NUMBER = "Le numéro de téléphone est incorrect.";
        public static string INCORRECT_PASSWORD = "Le mot de passe doit contenir au moins 8 caractères.";
    }
}