﻿using FluentValidation;
using PizzaIllico.Mobile.Dtos.Accounts;

namespace PizzaIllico.Mobile.Validations
{
    public class CreateUserRequestValidator : AbstractValidator<CreateUserRequest>
    {
        public CreateUserRequestValidator()
        {
            RuleFor(user => user.Email)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .EmailAddress()
                .WithMessage(ErrorMessages.INCORRECT_EMAIL);

            RuleFor(user => user.FirstName)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD);

            RuleFor(user => user.LastName)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD);
            
            RuleFor(user => user.PhoneNumber)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .Matches("^((06|07)\\d{8})|(\\+33(6|7)\\d{8})$")
                .WithMessage(ErrorMessages.INCORRECT_PHONE_NUMBER);;

            RuleFor(user => user.Password)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .MinimumLength(8)
                .WithMessage(ErrorMessages.INCORRECT_PASSWORD);;
        }
    }
}