﻿using FluentValidation;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;

namespace PizzaIllico.Mobile.Validations
{
    public class SetPasswordRequestValidator : AbstractValidator<SetPasswordRequest>
    {
        public SetPasswordRequestValidator()
        {
            RuleFor(credentials => credentials.OldPassword)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD);

            RuleFor(credentials => credentials.NewPassword)
                .NotNull()
                .WithMessage(ErrorMessages.REQUIRED_FIELD)
                .NotEmpty()
                .WithMessage(ErrorMessages.REQUIRED_FIELD);
        }
    }
}