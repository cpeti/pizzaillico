﻿using System;
using System.Globalization;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Converters
{
    public class ShopDistanceConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                return "Inconnue";
            }
            
            if (!(values[0] is Location l) || !(values[1] is ShopItem s))
            {
                return "Inconnue";
            }

            Location location = (Location)values[0];
            ShopItem shopItem = (ShopItem) values[1];
            return "Distance : " + Location.CalculateDistance(location.Latitude, location.Longitude, shopItem.Latitude, shopItem.Longitude, DistanceUnits.Kilometers) + " km";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}