﻿using System.Diagnostics;
using MonkeyCache.SQLite;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace PizzaIllico.Mobile
{
    public partial class App
    {
        public App() : base(() => new LoginPage(), RegisterServices)
        {
#if DEBUG
            Log.Listeners.Add(new DelegateLogListener((arg1, arg2) => Debug.WriteLine($"{arg1} : {arg2}")));
#endif
            InitializeComponent();
            Barrel.ApplicationId = "PizzaIllico";
        }

        private static void RegisterServices()
        {
            DependencyService.RegisterSingleton<IApiService>(new ApiService());
            
            DependencyService.RegisterSingleton<IPizzaApiService>(new PizzaApiService());
            
            DependencyService.RegisterSingleton<IOrderApiService>(new OrderApiService());
            
            DependencyService.RegisterSingleton<IAuthApiService>(new AuthApiService());
            
            DependencyService.RegisterSingleton<IUserApiService>(new UserApiService());
        }
    }
}