﻿using Xamarin.Forms.Maps;

namespace PizzaIllico.Mobile.CustomRenderers
{
    public class ShopPin : Pin
    {
        public long ShopId { get; set; }
    }
}