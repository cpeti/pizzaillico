using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PizzaIllico.Mobile.Services
{
    public interface IApiService
    {
        Task<TResponse> Get<TResponse>(string url, string token = "");
        Task<TResponse> Post<TResponse>(string url, string contentSerialized, string token = "");

        Task<TResponse> Patch<TResponse>(string url, string contentSerialized, string token);
    }
    
    public class ApiService : IApiService
    {
	    private const string HOST = "https://pizza.julienmialon.ovh/";
        private readonly HttpClient _client = new HttpClient();
        
        public async Task<TResponse> Get<TResponse>(string url, string token = "")
        {
	        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, HOST + url);

	        if (token != "")
	        {
		        request.Headers.Add("Authorization", token);
	        }

	        HttpResponseMessage response = await _client.SendAsync(request);

	        string content = await response.Content.ReadAsStringAsync();

	        return JsonConvert.DeserializeObject<TResponse>(content);
        }

        public async Task<TResponse> Post<TResponse>(string url, string contentSerialized, string token = "")
        {
	        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, HOST + url)
	        {
		        Content = new StringContent(contentSerialized, Encoding.UTF8, "application/json-patch+json")
	        };
	        
	        if (token != "")
	        {
		        request.Headers.Add("Authorization", token);
	        }

	        HttpResponseMessage response = await _client.SendAsync(request);
	        
	        string responseContent = await response.Content.ReadAsStringAsync();
	        return JsonConvert.DeserializeObject<TResponse>(responseContent);
        }

        public async Task<TResponse> Patch<TResponse>(string url, string contentSerialized, string token)
        {
	        HttpMethod method = new HttpMethod("PATCH");
	        HttpRequestMessage request = new HttpRequestMessage(method, HOST + url)
	        {
		        Content = new StringContent(contentSerialized, Encoding.UTF8, "application/json-patch+json")
	        };

	        request.Headers.Add("Authorization", token);
	        
	        HttpResponseMessage response = await _client.SendAsync(request);
	        
	        string responseContent = await response.Content.ReadAsStringAsync();
	        return JsonConvert.DeserializeObject<TResponse>(responseContent);
        }
    }
}