﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IAuthApiService
    {
        Task<Response<LoginResponse>> Login(LoginWithCredentialsRequest loginDetails);

        Task<Response<LoginResponse>> SetPassword(SetPasswordRequest setPasswordRequest, string token);

        Task<Response<LoginResponse>> RefreshToken(RefreshRequest refreshRequest);
    }

    public class AuthApiService : IAuthApiService
    {
        private readonly IApiService _apiService;

        public AuthApiService()
        {
            _apiService = DependencyService.Get<IApiService>();
        }

        public async Task<Response<LoginResponse>> Login(LoginWithCredentialsRequest loginDetails)
        {
            string loginSerialized = JsonConvert.SerializeObject(loginDetails);
            return await _apiService.Post<Response<LoginResponse>>(Urls.LOGIN_WITH_CREDENTIALS, loginSerialized);
        }

        public async Task<Response<LoginResponse>> SetPassword(SetPasswordRequest setPasswordRequest, string token)
        {
            string requestSerialized = JsonConvert.SerializeObject(setPasswordRequest);
            return await _apiService.Patch<Response<LoginResponse>>(Urls.SET_PASSWORD, requestSerialized, token);
        }

        public async Task<Response<LoginResponse>> RefreshToken(RefreshRequest refreshRequest)
        {
            string requestSerialized = JsonConvert.SerializeObject(refreshRequest);
            return await _apiService.Post<Response<LoginResponse>>(Urls.REFRESH_TOKEN, requestSerialized);
        }
    }
}