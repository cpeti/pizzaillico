using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IPizzaApiService
    {
        /**
         * Cherche la liste de tous les restaurants
         */
        Task<Response<List<ShopItem>>> ListShops();
        
        /**
         * Cherche la liste des pizzas du restaurant en parametres
         */
        Task<Response<List<PizzaItem>>> ListPizzas(int shopId);
    }
    
    public class PizzaApiService : IPizzaApiService
    {
        private readonly IApiService _apiService;

        public PizzaApiService()
        {
            _apiService = DependencyService.Get<IApiService>();
        }

        public async Task<Response<List<ShopItem>>> ListShops()
        {
	        return await _apiService.Get<Response<List<ShopItem>>>(Urls.LIST_SHOPS);
        }

        public async Task<Response<List<PizzaItem>>> ListPizzas(int shopId)
        {
            string url = Urls.LIST_PIZZA.Replace(Urls.SHOP_ID, shopId.ToString());
            return await _apiService.Get<Response<List<PizzaItem>>>(url);
        }
    }
}