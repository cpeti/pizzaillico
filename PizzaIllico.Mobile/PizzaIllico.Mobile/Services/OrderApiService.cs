﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IOrderApiService
    {
        Task<Response<List<OrderItem>>> ListOrders(string token);
        Task<Response<OrderItem>> DoOrder(long shopId, CreateOrderRequest orderRequest, string token);
    }
    public class OrderApiService: IOrderApiService
    {
        private readonly IApiService _apiService;


        public OrderApiService()
        {
            _apiService = DependencyService.Get<IApiService>();

        }

        public async Task<Response<List<OrderItem>>> ListOrders(string token)
        {
            return await _apiService.Get<Response<List<OrderItem>>>(Urls.LIST_ORDERS, token);
        }

        public async Task<Response<OrderItem>> DoOrder(long shopId, CreateOrderRequest orderRequest, string token)
        {
            string url = Urls.DO_ORDER.Replace(Urls.SHOP_ID, shopId.ToString());
            string orderSerialized = JsonConvert.SerializeObject(orderRequest);
            return await _apiService.Post<Response<OrderItem>>(url, orderSerialized, token);
        }
    }
}