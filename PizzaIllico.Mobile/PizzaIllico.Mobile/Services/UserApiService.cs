﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IUserApiService
    {
        Task<Response<LoginResponse>> Register(CreateUserRequest user);

        Task<Response<UserProfileResponse>> GetUserProfile(string token);

        Task<Response<UserProfileResponse>> SetUserProfile(SetUserProfileRequest setUserProfileRequest, string token);
    }
    public class UserApiService : IUserApiService
    {
        private readonly IApiService _apiService;

        public UserApiService()
        {
            _apiService = DependencyService.Get<IApiService>();
        }

        public async Task<Response<LoginResponse>> Register(CreateUserRequest user)
        {
            string userSerialized = JsonConvert.SerializeObject(user);
            return await _apiService.Post<Response<LoginResponse>>(Urls.CREATE_USER, userSerialized);
        }

        public async Task<Response<UserProfileResponse>> GetUserProfile(string token)
        {
            return await _apiService.Get<Response<UserProfileResponse>>(Urls.USER_PROFILE, token);
        }

        public async Task<Response<UserProfileResponse>> SetUserProfile(SetUserProfileRequest setUserProfileRequest, string token)
        {
            string profileSerialized = JsonConvert.SerializeObject(setUserProfileRequest);
            return await _apiService.Patch<Response<UserProfileResponse>>(Urls.SET_USER_PROFILE, profileSerialized, token);
        }
    }
}